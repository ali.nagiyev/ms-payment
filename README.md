<h1 align="center"> ms.payment </h1> <br>

## Table of Contents

- [Introduction](#Introduction)
- [How to run locally](#How-to-run-locally)
- [Tech stack](#Tech-stack)
- [Swagger](#Swagger-link)

## Introduction

This app's responsibility is to save user's payments and show them. When you purchase something from products , it save transaction.
Even if payment is failure, then it save transaction with ERROR message

## How to run locally

shell script
$ java -jar ms.payment.jar


## Tech stack
1. Spring Boot
2. Java 11

## Swagger link
[ms.auth](http://localhost:8085/swagger-ui/index.html#/
