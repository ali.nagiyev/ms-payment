package az.payment.mspayment.client;

import az.payment.mspayment.config.FeignConfig;
import az.payment.mspayment.model.dto.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@FeignClient(
        name = "ms-card",
        url = "localhost:8089/",
        configuration = FeignConfig.class
)
public interface CardClient {

    @PutMapping("/api/cards")
    void checkAndSubtractAmount(@RequestParam String cardUuid, @RequestParam BigDecimal amount);

    @PutMapping("/api/cards/reverse")
    void reverseBalance(@RequestParam String cardUuid, @RequestParam BigDecimal amount);

    @GetMapping("/api/cards/uuid")
    Map<String, String> getCardsByUuid(@RequestParam List<String> uuid);
}
