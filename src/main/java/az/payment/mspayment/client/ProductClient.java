package az.payment.mspayment.client;

import az.payment.mspayment.config.FeignConfig;
import az.payment.mspayment.model.dto.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(
        name = "ms-product",
        url = "localhost:8087/",
        configuration = FeignConfig.class
)
public interface ProductClient {

    @PutMapping("/api/products")
    void checkAndUpdateProductStockCount(@RequestParam String productUuid, @RequestParam Long productCount);

    @GetMapping("/api/products/uuid")
    Map<String,ProductDto> getProductByUuid(@RequestParam List<String> uuid);

}
