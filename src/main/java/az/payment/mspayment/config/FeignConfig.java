package az.payment.mspayment.config;

import az.payment.mspayment.config.decoder.PaymentErrorDecoder;
import az.payment.mspayment.config.logging.CustomLogging;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    public CustomLogging customLogging() {
        return new CustomLogging();
    }

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }

    @Bean
    public PaymentErrorDecoder errorDecoder() {
        return new PaymentErrorDecoder();
    }
}
