package az.payment.mspayment.config.decoder;

import az.payment.mspayment.exception.ClientException;
import az.payment.mspayment.model.enums.Language;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

import static az.payment.mspayment.model.constant.ExceptionConstants.*;
import static az.payment.mspayment.model.enums.Exceptions.getExceptionTranslation;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Slf4j
public class CustomErrorDecoder {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public JsonNode getJsonNode(Response response, String methodKey) {

        if (response.status() == UNAUTHORIZED.value())
            throw new ClientException(USER_UNAUTHORIZED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);

        try {
            return objectMapper.readValue(response.body().asInputStream(), JsonNode.class);
        } catch (Exception e) {
            throw new ClientException(getExceptionTranslation(CLIENT_EXCEPTION_CODE, Language.ENG), CLIENT_EXCEPTION_CODE, 503);
        }
    }
}
