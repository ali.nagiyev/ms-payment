package az.payment.mspayment.config.decoder;

import az.payment.mspayment.exception.ClientException;
import az.payment.mspayment.exception.InsufficientFundsException;
import az.payment.mspayment.exception.InsufficientStockException;
import az.payment.mspayment.model.enums.Language;
import com.fasterxml.jackson.databind.JsonNode;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static az.payment.mspayment.config.decoder.JsonNodeFieldName.*;
import static az.payment.mspayment.model.constant.ExceptionConstants.*;
import static az.payment.mspayment.model.enums.Exceptions.*;
import static az.payment.mspayment.model.enums.Language.AZE;

@Slf4j
public class PaymentErrorDecoder extends CustomErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        var requestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        HttpServletRequest request = null;
        if (requestAttributes != null) request = requestAttributes.getRequest();

        JsonNode jsonNode = getJsonNode(response, methodKey);

        String exceptionMessage;
        String exceptionCode;
        Language language = AZE;

        if (request != null) {
            var languageHeader = request.getHeader("Accept-Language");
            language = languageHeader != null ? Language.valueOf(languageHeader) : AZE;
        }

        if (jsonNode.has(DATA) && jsonNode.get(DATA).has(MESSAGE)) {

            exceptionMessage = jsonNode.get(DATA).get(MESSAGE).asText();
                exceptionCode = CLIENT_EXCEPTION_CODE;

            if (exceptionMessage.contains(INSUFFICIENT_FUNDS_CONTAINED_WORD))
                throw new InsufficientFundsException(
                        getExceptionTranslation(INSUFFICIENT_FUNDS_CODE, language), INSUFFICIENT_FUNDS_CODE);

            if (exceptionMessage.contains(INSUFFICIENT_STOCK_WORD))
                throw new InsufficientStockException(
                        getExceptionTranslation(INSUFFICIENT_STOCK_CODE, language), INSUFFICIENT_STOCK_CODE);

        } else if (jsonNode.has(MESSAGE) && jsonNode.has(CODE)) {
            exceptionMessage = jsonNode.get(MESSAGE).asText();
            exceptionCode = jsonNode.get(CODE).asText();

            if (exceptionMessage.contains(INSUFFICIENT_FUNDS_CONTAINED_WORD))
                throw new InsufficientFundsException(
                        getExceptionTranslation(INSUFFICIENT_FUNDS_CODE, language), INSUFFICIENT_FUNDS_CODE);

            if (exceptionMessage.contains(INSUFFICIENT_STOCK_WORD))
                throw new InsufficientStockException(
                        getExceptionTranslation(INSUFFICIENT_STOCK_CODE, language), INSUFFICIENT_STOCK_CODE);

        } else if (jsonNode.has(MESSAGE)) {

            exceptionMessage = jsonNode.get(MESSAGE).asText();
            exceptionCode = UNEXPECTED_EXCEPTION_CODE;

        } else {
            throw new ClientException(getExceptionTranslation(CLIENT_EXCEPTION_CODE, language), CLIENT_EXCEPTION_CODE, 503);
        }
        return new ClientException(exceptionMessage, exceptionCode, response.status());
    }
}
