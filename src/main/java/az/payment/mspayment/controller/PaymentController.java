package az.payment.mspayment.controller;

import az.payment.mspayment.model.request.PaymentRequest;
import az.payment.mspayment.model.response.PaymentResponse;
import az.payment.mspayment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static az.payment.mspayment.model.constant.Headers.AUTHORIZATION;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/payments")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PaymentController {

    PaymentService paymentService;

    @GetMapping
    public List<PaymentResponse> getUserTransactions(
            @RequestHeader(AUTHORIZATION) String accessToken,
            @PageableDefault Pageable pageable
    ) {
        return paymentService.getUserPaymentTransaction(accessToken, pageable);
    }

    @PostMapping
    public void savePaymentTransaction(
            @RequestHeader(AUTHORIZATION) String accessToken,
            @RequestBody PaymentRequest paymentRequest
    ) {
        paymentService.savePaymentTransaction(paymentRequest, accessToken);
    }
}
