package az.payment.mspayment.dao.repository;

import az.payment.mspayment.dao.entity.PaymentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Long> {

    List<PaymentEntity> findAllByUserUuid(String userUuid, Pageable pageable);
}
