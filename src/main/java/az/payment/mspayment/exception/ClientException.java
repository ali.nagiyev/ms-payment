package az.payment.mspayment.exception;

import lombok.Getter;

@Getter
public class ClientException extends RuntimeException {

    private final String code;
    private final int httpStatus;

    public ClientException(String message, String code, int httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }
}
