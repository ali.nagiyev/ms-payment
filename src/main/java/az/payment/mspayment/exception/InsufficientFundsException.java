package az.payment.mspayment.exception;

import lombok.Getter;

@Getter
public class InsufficientFundsException extends RuntimeException {

    private final String code;

    public InsufficientFundsException(String message, String code) {
        super(message);
        this.code = code;
    }
}
