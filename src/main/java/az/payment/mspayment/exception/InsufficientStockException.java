package az.payment.mspayment.exception;

import lombok.Getter;

@Getter
public class InsufficientStockException extends RuntimeException {
    private final String code;

    public InsufficientStockException(String message, String code) {
        super(message);
        this.code = code;
    }
}
