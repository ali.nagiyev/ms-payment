package az.payment.mspayment.model.constant;

public interface ExceptionConstants {
    String UNEXPECTED_EXCEPTION_CODE = "UNEXPECTED_EXCEPTION";
    String UNEXPECTED_EXCEPTION_MESSAGE = "Unexpected exception occurred";
    String BAD_INPUT_CODE = "BAD_INPUT";
    String CLIENT_EXCEPTION_CODE = "CLIENT_FAILED";
    String CLIENT_EXCEPTION_MESSAGE = "Error when deserialize error response for method:%s";
    String NGBSS_EXCEPTION_CODE = "NGBSS_ERROR";
    String PACKAGE_NOT_FOUND_CODE = "PACKAGE_NOT_FOUND";
    String PACKAGE_NOT_FOUND_MESSAGE = "Package with id:%s not found for user:%s";
    String PACKAGE_NOT_COMPATIBLE_CODE = "PACKAGE_NOT_COMPATIBLE";
    String PACKAGE_NOT_COMPATIBLE_MESSAGE = "Package not compatible with you tariff";
    String SUBSCRIBER_BLOCKED_CODE = "SUBSCRIBER_BLOCKED";
    String SUBSCRIBER_BLOCKED_MESSAGE = "Сan not activate package for a blocked number";
    String USER_UNAUTHORIZED_CODE = "USER_UNAUTHORIZED";
    String USER_UNAUTHORIZED_MESSAGE = "User unauthorized";
    String INSUFFICIENT_FUNDS_CODE = "INSUFFICIENT_FUNDS";
    String INSUFFICIENT_STOCK_CODE = "INSUFFICIENT_STOCK";
    String INSUFFICIENT_FUNDS_MESSAGE = "You don't have sufficient funds for this operation";
    String MY_HOUR_PACKAGE_LIMIT_EXCEED_CODE = "MY_HOUR_PACKAGE_EXCEED";
    String MY_HOUR_PACKAGE_LIMIT_EXCEED_MESSAGE = "The count of my hour package exceed the max number 1";
    String PENDING_BUSINESS_ORDERS_MESSAGE = "The subscriber has pending business orders";
    String PENDING_BUSINESS_ORDERS_CODE = "PENDING_BUSINESS_ORDERS";
    String MY_HOUR_PACKAGE_NOT_ALLOWED_NOW_CODE = "MY_HOUR_PACKAGE_NOT_ALLOWED_NOW";
    String INSUFFICIENT_FUNDS_CONTAINED_WORD = "There is insufficient balance";
    String INSUFFICIENT_STOCK_WORD = "There is no this count in stock";
}
