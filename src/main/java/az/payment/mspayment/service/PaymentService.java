package az.payment.mspayment.service;

import az.payment.mspayment.client.AuthClient;
import az.payment.mspayment.client.CardClient;
import az.payment.mspayment.client.ProductClient;
import az.payment.mspayment.dao.entity.PaymentEntity;
import az.payment.mspayment.dao.repository.PaymentRepository;
import az.payment.mspayment.exception.InsufficientFundsException;
import az.payment.mspayment.exception.InsufficientStockException;
import az.payment.mspayment.model.dto.ProductDto;
import az.payment.mspayment.model.request.PaymentRequest;
import az.payment.mspayment.model.request.VerifyTokenRequest;
import az.payment.mspayment.model.response.PaymentResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static az.payment.mspayment.model.enums.Status.*;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final ProductClient productClient;
    private final CardClient cardClient;
    private final AuthClient authClient;

    public List<PaymentResponse> getUserPaymentTransaction(String accessToken, Pageable pageable) {

        var userUuid = authClient.verifyAccessToken(VerifyTokenRequest.of(accessToken)).getUserUUID();

        var paymentEntityList = paymentRepository.findAllByUserUuid(userUuid, pageable);

        var productUUIDs = paymentEntityList.stream().map(PaymentEntity::getProductUuid).collect(toList());

        var cardUUIDs = paymentEntityList.stream().map(PaymentEntity::getCardUuid).collect(toList());

        var mapCards = cardClient.getCardsByUuid(cardUUIDs);

        var mapProducts = productClient.getProductByUuid(productUUIDs);

        return paymentEntityList.stream()
                .map(
                        payment -> PaymentResponse.of(mapCards.get(payment.getCardUuid()), mapProducts.get(payment.getProductUuid()).getName(), mapProducts.get(payment.getProductUuid()).getPrice().doubleValue())

                )
                .collect(toList());
    }

    public void savePaymentTransaction(PaymentRequest paymentRequest, String accessToken) {

        var userUuid = authClient.verifyAccessToken(VerifyTokenRequest.of(accessToken)).getUserUUID();

        var paymentModel = PaymentEntity.builder()
                .userUuid(userUuid)
                .cardUuid(paymentRequest.getCardUuid())
                .productUuid(paymentRequest.getProductUuid())
                .paymentUuid(UUID.randomUUID().toString())
                .build();

        try {
            cardClient.checkAndSubtractAmount(paymentRequest.getCardUuid(), BigDecimal.valueOf(paymentRequest.getPrice() * paymentRequest.getProductCount()));

            productClient.checkAndUpdateProductStockCount(paymentRequest.getProductUuid(), paymentRequest.getProductCount());

            paymentModel.setStatus(SUCCESS);
            paymentRepository.save(paymentModel);

        } catch (InsufficientFundsException e) {

            paymentModel.setStatus(ERROR);
            paymentRepository.save(paymentModel);
            throw new RuntimeException();

        } catch (InsufficientStockException e2) {

            cardClient.reverseBalance(paymentRequest.getCardUuid(), BigDecimal.valueOf(paymentRequest.getPrice() * paymentRequest.getProductCount()));
            paymentModel.setStatus(ERROR);
            paymentRepository.save(paymentModel);
            throw new RuntimeException();

        }
    }
}
